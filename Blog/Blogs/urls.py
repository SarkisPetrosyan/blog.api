from .views import BlogListAPIView, BlogDetailsAPIView
from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns

urlpatterns = [
    path('blogs', BlogListAPIView.as_view()),
    path('blogs/<int:pk>/', BlogDetailsAPIView.as_view()),

]

urlpatterns = format_suffix_patterns(urlpatterns)