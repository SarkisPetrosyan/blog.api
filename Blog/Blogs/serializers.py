from .models import Blog
from rest_framework import serializers


class BlogCreateSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    Author = serializers.CharField(max_length=30)
    Title = serializers.CharField(max_length=30)
    Description = serializers.CharField(max_length=30),
    Photo = serializers.FileField()
    Views = serializers.IntegerField()

    def create(self, validated_data):
        return Blog.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.title = validated_data.get('title', instance.title)
        instance.code = validated_data.get('code', instance.code)
        instance.linenos = validated_data.get('linenos', instance.linenos)
        instance.language = validated_data.get('language', instance.language)
        instance.style = validated_data.get('style', instance.style)
        instance.save()
        return instance


class BlogListSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    Author = serializers.CharField(max_length=30)
    Title = serializers.CharField(max_length=30)
    Description = serializers.CharField(max_length=30),
    Photo = serializers.FileField()
    Views = serializers.IntegerField()

    def create(self, validated_data):
        return Blog.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.title = validated_data.get('title', instance.title)
        instance.code = validated_data.get('code', instance.code)
        instance.linenos = validated_data.get('linenos', instance.linenos)
        instance.language = validated_data.get('language', instance.language)
        instance.style = validated_data.get('style', instance.style)
        instance.save()
        return instance
