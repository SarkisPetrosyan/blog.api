from .views import FollowerCreateAPIView, FollowerListAPIView
from django.urls import path

urlpatterns = [
    path('create', FollowerCreateAPIView.as_view()),
    path('all', FollowerListAPIView.as_view()),
]
