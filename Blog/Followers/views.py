from .models import Follower
from .serializers import FollowerCreateSerializer, FollowerListSerializer
from rest_framework import generics
# Create your views here.

class FollowerCreateAPIView(generics.CreateAPIView):
    queryset = Follower.objects.all()
    serializer_class = FollowerCreateSerializer

class FollowerListAPIView(generics.ListAPIView):
    queryset = Follower.objects.all()
    serializer_class = FollowerListSerializer
