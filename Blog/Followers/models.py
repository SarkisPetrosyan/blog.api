from django.db import models

from Users.models import User
from Blogs.models import Blog


class Follower(models.Model):
    UserName = models.ForeignKey(User, on_delete=models.CASCADE)
    Author = models.ForeignKey(Blog, on_delete=models.CASCADE)
