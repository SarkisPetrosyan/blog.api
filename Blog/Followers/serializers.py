from .models import Follower
from rest_framework import serializers

class FollowerCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Follower
        fields = '__all__'

class FollowerListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Follower
        fields = '__all__'