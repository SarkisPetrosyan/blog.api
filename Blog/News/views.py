from .models import News
from .serializers import NewsCreateSerializer, NewsListSerializer,NewsDetailSerializer
from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status



class NewsCreateAPIView(APIView):
    
    def post(self, request, format=None):
        serializer = Serializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class NewsListAPIView(APIView):

     def get(self, request, format=None):
        News = News.objects.all()
        serializer = NewsSerializer(News, many=True)
        return Response(serializer.data)
    

class NewsDetailAPIView(APIView):

    def get_object(self, pk):
        try:
            return News.objects.get(pk=pk)
        except News.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        News = self.get_object(pk)
        serializer = NewsSerializer(News)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        News = self.get_object(pk)
        serializer = NewsSerializer(News, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        News = self.get_object(pk)
        News.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)