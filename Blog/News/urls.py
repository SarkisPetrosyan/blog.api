from .views import NewsCreateAPIView, NewsListAPIView,NewsDetailAPIView
from django.urls import path

urlpatterns = [

    path('create', NewsCreateAPIView.as_view()),
    path('all', NewsListAPIView.as_view()),
    path('detail/<int:pk>', NewsDetailAPIView.as_view()),
    

]
