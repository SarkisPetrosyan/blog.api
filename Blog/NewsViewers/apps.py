from django.apps import AppConfig


class NewsviewersConfig(AppConfig):
    name = 'NewsViewers'
