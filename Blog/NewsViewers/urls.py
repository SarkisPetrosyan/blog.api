from .views import NewsViewersCreateAPIView, NewsViewersListAPIView,NewsViewersDetailAPIView
from django.urls import path

urlpatterns = [

    path('create', NewsViewersCreateAPIView.as_view()),
    path('all', NewsViewersListAPIView.as_view()),
    path('detail/<int:pk>', NewsViewersDetailAPIView.as_view()),
    

]
