from .models import NewsViewers
from rest_framework import serializers


class NewsViewersCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = NewsViewers
        fields = '__all__'

class NewsViewersListSerializer(serializers.ModelSerializer):
    class Meta:
        model = NewsViewers
        fields = '__all__'

class NewsViewersDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = NewsViewers
        fields = '__all__'        