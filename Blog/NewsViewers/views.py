from .models import NewsViewers
from .serializers import NewsViewersCreateSerializer, NewsViewersListSerializer,NewsViewersDetailSerializer
from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status


class NewsViewersCreateAPIView(APIView):
    def post(self,request,format=None):
        serializer = serializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)    

    
    
class NewsViewersListAPIView(APIView):
    def get(self,request,format=None):
        NewsViewers = NewsViewers.objects.all()
        serializer = NewsViewersSerializer(NewsViewers,many=True)
        return Response(serializer.data)

    

class NewsViewersDetailAPIView(APIView):
    def get_object(self,pk):
        try:
            return NewsViewers.object.get(pk=pk)
        except NewsViewers.DoesNotExist:
            raise Http404
    
    def get(self,request,pk,format=None):
        NewsViewers = self.get_object(pk)
        serializer = NewsViewersSerializer(NewsViewers)
        return Response(serializer.data)

    def put(self,request,pk,format=None):
        NewsViewers = self.get_object(pk)
        serializer = NewsViewersSerializer(NewsViewers, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)    

    def delete(self,request,pk,formay=None):
        NewsViewers = self.get_object(pk)
        NewsViewers.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)