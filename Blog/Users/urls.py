from .views import UserCreateAPIView, UserListAPIView
from django.urls import path

urlpatterns = [
    path('create', UserCreateAPIView.as_view()),
    path('all', UserListAPIView.as_view()),
]
