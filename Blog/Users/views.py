from django.shortcuts import render

# Create your views here.
from .models import User
from .serializers import UserCreateSerializer, UserListSerializer
from rest_framework import generics
# Create your views here.

class UserCreateAPIView(generics.CreateAPIView):
    queryset = User.objects.all()
    serializer_class = UserCreateSerializer

class UserListAPIView(generics.ListAPIView):
    queryset = User.objects.all()
    serializer_class = UserListSerializer
