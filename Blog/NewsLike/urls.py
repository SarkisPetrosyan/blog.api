from .views import NewsLikesCreateAPIView, NewsLikesListAPIView,NewsLikesDetailAPIView
from django.urls import path

urlpatterns = [

    path('create', NewsLikesCreateAPIView.as_view()),
    path('all', NewsLikesListAPIView.as_view()),
    path('detail/<int:pk>', NewsLikesDetailAPIView.as_view()),
    

]
