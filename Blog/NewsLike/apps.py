from django.apps import AppConfig


class NewslikeConfig(AppConfig):
    name = 'NewsLike'
