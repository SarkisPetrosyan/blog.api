from .models import NewsLikes
from rest_framework import serializers


class NewsLikesCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = NewsLikes
        fields = '__all__'

class NewsLikesListSerializer(serializers.ModelSerializer):
    class Meta:
        model = NewsLikes
        fields = '__all__'

class NewsLikesDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = NewsLikes
        fields = '__all__'        